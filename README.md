# Démonstration d'intégration continue avec GitLab sur un projet C++

Ce projet d'exemple C++ contient une application "Hello World" ainsi qu'un test unitaire.

## Environnement de compilation

Le projet nécessite CMake pour être généré (version 3.2 minimum).
Un compulateur C++14 est aussi nécessaire (testé avec `g++`).

Pour installer ces outils sous Ubuntu 16.04, taper :

```bash
apt-get install -y build-essential cmake
```

## Compilation et exécution des tests unitaires

```bash
cd cpp-demo
mkdir build && cd build
cmake ..
make
make test
```

## Pipeline status
Markdown
```bash
[![pipeline status](https://gitlab.com/ilaipaconlui/gitlabci-demo-cpp/badges/master/pipeline.svg)](https://gitlab.com/ilaipaconlui/gitlabci-demo-cpp/commits/master)
```